# test-nix-dev-shells

From the project root,



```
nix-shell --pure
```

Say yes to everything. Ignore all warnings. You should then have a nix environment that can run

```
cabal run
```

and see

```
Hello, Haskell!
```

If you see an error about a missing lock file, please do the following:

```
nix --experimental-features "nix-command flakes" develop
```

That should produce a `flake.lock` file.

Then try `nix-shell --pure` again.

