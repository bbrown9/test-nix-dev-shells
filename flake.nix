{
  # Cribbed from 
  # https://input-output-hk.github.io/haskell.nix/tutorials/getting-started-flakes.html 
  description = "A dev shell for haskell";
  inputs.haskellNix.url = "github:input-output-hk/haskell.nix";
  inputs.nixpkgs.follows = "haskellNix/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  # For use with nix-shell without using flakes
  inputs.flake-compat = {
    url = "github:edolstra/flake-compat";
    flake = false;
  };
  outputs = { self, nixpkgs, flake-utils, haskellNix, flake-compat }:
    let
      supportedSystems =
        [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];
    in flake-utils.lib.eachSystem supportedSystems (system:
      let
        overlays = [
          haskellNix.overlay
          (final: prev: {
            # This overlay adds our project to pkgs
            helloProject = final.haskell-nix.project {
              src = ./.;
              # TODO EDIT ME! If on an M1 mac, should be aarch64-darwin
              #evalSystem = "x86_64-linux";
              evalSystem = "aarch64-darwin";
              compiler-nix-name = "ghc924";
              shell.tools = {
                cabal = "3.6.2.0";
                hlint = "latest";
                # TODO uncommenting this, even using "latest" causes GHC to be built.
                # However it might be worth it if you do it once and it is cached.
                # haskell-language-server = "1.8.0.0";
              };
              #shell.buildInputs = with pkgs; [ nixpkgs-fmt ];
            };
          })
        ];
        pkgs = import nixpkgs {
          inherit system overlays;
          inherit (haskellNix) config;
        };
        flake = pkgs.helloProject.flake { };
      in flake // {
        # Not used. Just for shell
        # Built by `nix build .`
        # packages.default = flake.packages."hello:exe:hello";
        #legacyPackages = pkgs;
      });
  # --- Flake Local Nix Configuration ----------------------------
  nixConfig = {
    # This sets the flake to use the IOG nix cache.
    # Nix should ask for permission before using it,
    # but remove it here if you do not want it to.
    extra-substituters = [ "https://cache.iog.io" ];
    extra-trusted-public-keys =
      [ "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ=" ];
    allow-import-from-derivation = "true";
  };
}
